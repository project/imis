<?php

namespace Drupal\imis;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Url;
use Drupal\key\KeyRepositoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * A client to the iMIS API.
 */
class ImisClient {

  /**
   * The HTTP request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * HTTP Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The domain name to which to make requests.
   *
   * @var string
   */
  protected $domain;

  /**
   * The path, following $domain, to the iMIS login page.
   *
   * @var string
   */
  protected $loginPath;

  /**
   * The iMIS client ID.
   *
   * @var string
   */
  protected $clientId;

  /**
   * The iMIS client secret.
   *
   * @var string
   */
  protected $clientSecret;

  /**
   * The page to which we should redirect the successfully logged-in visitor.
   *
   * @var string
   */
  protected $sourcePage;

  /**
   * The access token from the iMIS authentication server.
   *
   * @var string
   */
  protected $accessToken;

  /**
   * The authenticated iMIS user currently visiting our site.
   *
   * @var string
   */
  protected $imisUsername;

  /**
   * The response from the iMIS API's /token endpoint.
   *
   * @var object
   */
  protected $tokenResponse;

  /**
   * The response from the iMIS API's /user endpoint.
   *
   * @var object
   */
  protected $userResponse;

  /**
   * The response from the iMIS API's /party endpoint.
   *
   * @var object
   */
  protected $partyResponse;

  /**
   * {@inheritdoc}
   */
  public function __construct(RequestStack $request_stack, ClientFactory $http_client_factory, ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository) {
    $this->request = $request_stack->getCurrentRequest();
    $this->httpClient = $http_client_factory->fromOptions();
    $settings = $config_factory->get('imis.settings');
    $this->domain = $settings->get('imis_domain');
    $this->loginPath = $settings->get('imis_login_path');
    $imis_creds = json_decode($key_repository->getKey('imis')->getKeyValue());
    $this->clientId = $imis_creds->client_id;
    $this->clientSecret = $imis_creds->client_secret;
    $this->sourcePage = '/';
    $this->establishConnection();
  }

  /**
   * Do the initial handshake with iMIS and grab the values that comes with.
   */
  private function establishConnection() {
    if ($this->request->getMethod() == 'POST' && $refresh_token = $this->request->get('refresh_token')) {
      // Gross, but as far as I can tell the only way to get source_page.
      preg_match('/source_page=([^&]+)/', $this->request->server->get('HTTP_REFERER'), $matches);
      if (!empty($matches[1])) {
        $this->sourcePage = urldecode($matches[1]);
      }
      $form_params = [
        'grant_type' => 'refresh_token',
        'refresh_token' => $refresh_token,
        'client_id' => $this->clientId,
        'client_secret' => $this->clientSecret,
      ];
      $this->apiCall('token', $form_params);
      [$this->imisUsername, $this->accessToken] = $this->getValues(['UserName', 'access_token']);
      if ($this->imisUsername && $this->accessToken) {
        $this->getUserObject();
        $this->getPartyObject();
      }
    }
  }

  /**
   * Make any type of call to the iMIS API.
   *
   * @param string $type
   *   Call type: token, user, or party.
   * @param string|array $args
   *   Argument(s) to be sent along with this call.
   *
   * @return object|bool
   *   The API response, or FALSE.
   */
  private function apiCall(string $type, $args) {
    $url = 'https://' . $this->domain;
    $standard_headers = [
      'Content-Type' => 'application/json',
      'Authorization' => 'bearer ' . $this->accessToken,
    ];

    switch ($type) {
      case 'token':
        $url .= '/token';
        $response = $this->httpClient->post($url, ['form_params' => $args]);
        break;

      case 'user':
        $url .= '/api/user?username=' . $args;
        $response = $this->httpClient->get($url, ['headers' => $standard_headers]);
        break;

      case 'party':
        $url .= '/api/party/' . $args;
        $response = $this->httpClient->get($url, ['headers' => $standard_headers]);
        break;
    }

    if ($object = $response->getBody()->getContents() ?: FALSE) {
      $object = json_decode($object);
    }

    $this->{$type . "Response"} = $object;

    return $object;
  }

  /**
   * Extracts values from the objects iMIS's API responds with.
   *
   * @param array|string $values
   *   The names of the desired value(s).
   *
   * @return array|string
   *   The desired value(s).
   */
  public function getValues($values) {
    if (!is_array($values)) {
      $values = [$values];
      $return_single = TRUE;
    }
    else {
      $return_single = FALSE;
    }
    // Make the expect value names into keys.
    $values = array_flip($values);
    foreach ($values as $value_name => $value) {
      $value = NULL;
      switch ($value_name) {
        case 'UserName':
          $values[$value_name] = $this->tokenResponse->userName ?? NULL;
          break;

        case 'access_token':
          $values[$value_name] = $this->tokenResponse->access_token ?? NULL;
          break;

        case 'party_id':
          $values[$value_name] = $this->userResponse->Items->{'$values'}[0]->Party->Id ?? NULL;
          break;

        case 'is_member_record':
          $values[$value_name] = FALSE;
          if (!empty($this->partyResponse->AdditionalAttributes->{'$values'})) {
            foreach ($this->partyResponse->AdditionalAttributes->{'$values'} as $attribute) {
              if (!empty($attribute->Name) && $attribute->Name == 'IsMemberRecord' && !empty($attribute->Value->{'$value'})) {
                $values[$value_name] = TRUE;
                break;
              }
            }
          }
          break;

        case 'party_status_id':
          $values[$value_name] = $this->partyResponse->Status->PartyStatusId ?? NULL;
          break;

        case 'PrimaryEmail':
          // Note that this prefers the "primary" email address.
          if (!empty($this->partyResponse->Emails->{'$values'}) && is_array($this->partyResponse->Emails->{'$values'})) {
            foreach ($this->partyResponse->Emails->{'$values'} as $email_value) {
              if ($email_value->EmailType == '_Primary') {
                $values[$value_name] = $email_value->Address;
              }
            }
          }
          break;

        case 'JoinDate':
          if (!empty($this->partyResponse->AdditionalAttributes->{'$values'}) && is_array($this->partyResponse->AdditionalAttributes->{'$values'})) {
            foreach ($this->partyResponse->AdditionalAttributes->{'$values'} as $attribute_value) {
              if ($attribute_value->Name == 'JoinDate') {
                // Suitable format for a Drupal Date field.
                $values[$value_name] = date('Y-m-d', strtotime($attribute_value->Value));
              }
            }
          }
          break;

        case 'PrimaryOrganization':
          $values[$value_name] = $this->partyResponse->PrimaryOrganization->Name ?? NULL;
          break;

        case 'Title':
          $values[$value_name] = $this->partyResponse->PrimaryOrganization->Title ?? NULL;
          break;

        case 'FirstName':
        case 'LastName':
        case 'InformalName':
        case 'FullName':
        case 'NamePrefix':
          $values[$value_name] = $this->partyResponse->PersonName->{$value_name};
          break;

        default:
          $values[$value_name] = $this->partyResponse->{$value_name} ?? NULL;
      }
    }
    // Don't need the keys anymore.
    $values = array_values($values);
    return $return_single ? current($values) : $values;
  }

  /**
   * Get the page iMIS thinks we should redirect a logged-in user to.
   *
   * @return string
   *   The page's path (assumed to be on our site).
   */
  public function getSourcePage() {
    return $this->sourcePage;
  }

  /**
   * Get the iMIS username.
   *
   * @return string
   *   The username.
   */
  public function getUsername() {
    return $this->imisUsername;
  }

  /**
   * Get the iMIS user object corresponding to the current user.
   *
   * @return bool|object
   *   The user object.
   */
  private function getUserObject() {
    return $this->userResponse ?? $this->apiCall('user', $this->imisUsername);
  }

  /**
   * Get the iMIS party object corresponding to the current user.
   *
   * @return bool|object
   *   The party object.
   */
  private function getPartyObject() {
    if (empty($this->partyResponse)) {
      $this->getUserObject();
      $party_id = $this->getValues('party_id');
      $this->apiCall('party', $party_id);
    }
    return $this->partyResponse;
  }

  /**
   * Get the current user's primary email address.
   *
   * @return string
   *   The primary email address.
   */
  public function getPrimaryEmail() {
    return $this->getValues('PrimaryEmail');
  }

  /**
   * Get the URL for the iMIS login page.
   *
   * @param string $source_page
   *   An optional string, starting with '/', representing the source_page to
   *   return to after login.
   *
   * @return \Drupal\Core\Url
   *   The Url object.
   */
  public function getLoginUrl($source_page = '') {
    $url_options = [
      'absolute' => TRUE,
      'https' => TRUE,
      'query' => [
        'source_page' => $source_page ?: $this->sourcePage,
      ],
    ];
    return Url::fromUri('https://' . $this->domain . $this->loginPath, $url_options);
  }

}
