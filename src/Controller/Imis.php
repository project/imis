<?php

namespace Drupal\imis\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\externalauth\ExternalAuthInterface;
use Drupal\imis\ImisClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Controller that logs appropriate authenticated iMIS users into Drupal.
 */
class Imis extends ControllerBase {

  /**
   * The iMIS client, to abstract and simplify API calls.
   *
   * @var \Drupal\imis\ImisClient
   */
  protected $imisClient;

  /**
   * The service provided by the Drupal External Authentication module.
   *
   * @var \Drupal\externalauth\ExternalAuthInterface
   */
  protected $externalAuth;

  /**
   * {@inheritdoc}
   */
  public function __construct(ImisClient $imis_client, ExternalAuthInterface $external_auth) {
    $this->imisClient   = $imis_client;
    $this->externalAuth = $external_auth;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('imis.imisclient'),
      $container->get('externalauth.externalauth'),
      $container->get('module_handler')
    );
  }

  /**
   * A function to check the iMIS user's status and log them into Drupal.
   */
  public function login() {
    $prevent_login = $this->moduleHandler()->invokeAll('imis_prevent_login', [$this->imisClient]);
    if (array_filter($prevent_login)) {
      // Redirect to "Instructions on how to reactivate membership" page.
      $this->messenger()->addMessage($this->t('You need to start or renew your membership.'));
      $redirect_nid = $this->config('imis.settings')->get('membership_page');
      $redirect_page = $redirect_nid ? '/node/' . $redirect_nid : '/';
    }
    else {
      // Create/log in Drupal user.
      $user_field_map = $this->getUserFieldMap();
      $user_values = $this->getUserValues($user_field_map);
      $provider = 'iMIS';
      $user = $this->externalAuth->loginRegister($user_values['name'], $provider, $user_values);
      // Check whether we need to update the user to match iMIS.
      $user_updated = FALSE;
      foreach ($user_field_map as $field_name => $field) {
        if ($field['update'] && $user->hasField($field_name) && $user->get($field_name)->value != $user_values[$field_name]) {
          $user->set($field_name, $user_values[$field_name]);
          $user_updated = TRUE;
        }
      }
      if ($user_updated) {
        $user->save();
      }
      // Say hi and send them on their way.
      $this->messenger()->addMessage($this->t('Welcome, @username!', ['@username' => $user_values['name']]));
      $redirect_page = $this->imisClient->getSourcePage();
    }
    return new RedirectResponse($redirect_page);
  }

  /**
   * Get the map of Drupal fields to their intended iMIS or calculated values.
   *
   * See documentation for hook_imis_user_field_map_alter() in imis.api.php.
   *
   * @return array
   *   The field map (no values yet).
   */
  protected function getUserFieldMap() {
    $user_field_map = [
      'name' => [
        'type' => 'imis_value',
        'source' => 'UserName',
        'update' => TRUE,
      ],
      'mail' => [
        'type' => 'imis_value',
        'source' => 'PrimaryEmail',
        'update' => TRUE,
      ],
    ];
    $this->moduleHandler()->alter('imis_user_field_map', $user_field_map, $this->imisClient);
    $defaults = [
      'type' => 'imis_value',
      'update' => TRUE,
    ];
    foreach ($user_field_map as $field_name => &$field) {
      // Establish defaults in case someone passed in simple strings or an
      // incomplete array.
      if (!is_array($field)) {
        $field = ['source' => $field] + $defaults;
      }
      else {
        foreach ($defaults as $key => $default_value) {
          $field[$key] ?? $default_value;
        }
      }
    }
    return $user_field_map;
  }

  /**
   * Get the incoming user's values.
   *
   * See getUserFieldMap() docs for details.
   *
   * @param array $user_field_map
   *   The "instructions" for calculating user values.
   *
   * @return array
   *   The user values to send to Drupal, in a simple key => value array.
   */
  protected function getUserValues(array $user_field_map) {
    $user_values = [];
    foreach ($user_field_map as $field_name => &$field) {
      if ($field['type'] == 'calculated') {
        // Only calculated fields have their value pre-filled.
        $user_values[$field_name] = $field['value'];
      }
      else {
        $user_values[$field_name] = $this->imisClient->getValues($field['source']);
      }
    }
    return $user_values;
  }

}
