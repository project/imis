<?php

namespace Drupal\imis\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a configuration form for the iMIS connection.
 */
class ImisConfig extends ConfigFormBase {

  /**
   * Drupal entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['imis.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imis_imis_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('imis.settings');

    $form['imis_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $settings->get('imis_domain'),
      '#description' => $this->t('E.g. <em>sso.example.org</em>'),
      '#required' => TRUE,
    ];

    $default_nid = $settings->get('membership_page');
    $default_node = $default_nid ? $this->entityTypeManager->getStorage('node')->load($default_nid) : NULL;
    $form['membership_page'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Membership page'),
      '#description' => $this->t('The page to send inactive iMIS users, to entice them to register.'),
      '#target_type' => 'node',
      '#default_value' => $default_node,
    ];

    $form['imis_login_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('iMIS login path'),
      '#description' => $this->t('The path, starting with a forward slash, of the page on <strong>Domain</strong> to which users should be sent to log into iMIS.'),
      '#default_value' => $settings->get('imis_login_path'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('imis.settings')
      ->set('imis_domain', $form_state->getValue('imis_domain'))
      ->set('membership_page', $form_state->getValue('membership_page'))
      ->set('imis_login_path', $form_state->getValue('imis_login_path'))
      ->save();
  }

}
