<?php

/**
 * @file
 * Hooks and documentation related to iMIS module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Decide whether to prevent this iMIS user from being logged into Drupal.
 *
 * @param \Drupal\imis\ImisClient $imis_client
 *   An object representing the current connection to iMIS. Includes the
 *   current iMIS user's information, accessible through
 *   ImisClient::getPartyObject() and ImisClient::getUserObject().
 *
 * @return bool
 *   TRUE if we should prevent, FALSE otherwise.
 */
function hook_imis_prevent_login(\Drupal\imis\ImisClient $imis_client) {
  // The authenticated role is for mature audiences only.
  $birth_year = date_parse($imis_client->getValues('BirthDate'))['year'];
  return (date('Y') - $birth_year < 18);
}

/**
 * Establish or change map of Drupal user entity field names to iMIS ones.
 *
 * This returns an array where the keys are Drupal user entity field names, and
 * the values can either be strings or associative arrays.
 *
 * String values are expected to be iMIS source fields. These values will be
 * automatically updated if the incoming iMIS values differ from an existing
 * Drupal user's.
 *
 * Array values can have 4 possible keys:
 * - "type" (required) can contain one of two values:
 *   - "imis_value" means that this value should come from iMIS, from a
 *     "source" field (see "source" below) that ImisClient::getValues() will
 *     recognize.
 *   - "calculated" means that a developer implementing
 *     hook_imis_user_fields_map_alter() will calculate this value and place
 *     it in the "value" key (see "value" below).
 * - "source" (required if "type" == "imis_value") is the iMIS source field.
 * - "value" (required if "type" == "calculated") the value calculated in
 *   hook_imis_user_fields_map_alter().
 * - "update" (optional): whether to update an existing Drupal user if their
 *   values differ from those coming from getUserValues().
 */
function hook_imis_user_field_map_alter(&$user_fields, \Drupal\imis\ImisClient $imis_client) {
  $user_fields['field_first_name'] = 'FirstName';
  $user_fields['field_last_name'] = 'LastName';
  $user_fields['field_join_date'] = 'JoinDate';
}

/**
 * @} End of "addtogroup hooks".
 */
